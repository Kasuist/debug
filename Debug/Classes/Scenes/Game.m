//
//  Game.m
//  Debug
//
//  Created by Beau Young on 24/01/2014.
//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import "Game.h"
#import "SKButtonNode.h"
#import "TextureAtlasHelper.h"
#import "GameData.h"
#import "HireStaff.h"
#import "Upgrades.h"

#define kHUD_BAR_HEIGHT 60;

static NSString *fontName = @"PF Arma Five";

@implementation Game {
    SKTextureAtlas *textures;
    SKNode *hudLayerNode;
    SKButtonNode *squashButton;
    
    SKLabelNode *bugsLabel;
    SKLabelNode *bugsPerSecondLabel;
    
    NSTimer *timer;
}

- (void)didMoveToView:(SKView *)view {
    self.scaleMode = SKSceneScaleModeAspectFill;
    self.backgroundColor = [SKColor whiteColor];
    
    textures = [SKTextureAtlas atlasNamed:@"game"];
    [self createSceneContents];
}

- (void)createSceneContents {
    hudLayerNode = [SKNode node];
    [self addChild:hudLayerNode];
    [self setupUI];
    
    [self addChild:[self squashBugsButton]];
    [self addChild:[self hireStaffButton]];
    [self addChild:[self upgradeButton]];
    
    [self startTimer];
}

- (void)setupUI {
    int barHeight = kHUD_BAR_HEIGHT;
    CGSize backgroundSize = CGSizeMake(self.size.width, barHeight);
    SKSpriteNode *hudBarBackground = [SKSpriteNode spriteNodeWithColor:[UIColor colorWithRed:0.103 green:0.529 blue:0.882 alpha:0.740] size:backgroundSize];
    hudBarBackground.position = CGPointMake(0, self.size.height - barHeight);
    hudBarBackground.anchorPoint = CGPointZero;
    [hudLayerNode addChild:hudBarBackground];
    
    bugsLabel = [SKLabelNode labelNodeWithFontNamed:fontName];
    bugsLabel.fontSize = 20.0;
    bugsLabel.text = [NSString stringWithFormat:@"Bugs Squashed: %ld", (long)[GameData sharedData].bugs];
    bugsLabel.name = @"scoreLabel";
    bugsLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    bugsLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
    bugsLabel.position = CGPointMake(CGRectGetMidX(self.frame), self.size.height - bugsLabel.frame.size.height);
    [hudLayerNode addChild:bugsLabel];
    
    bugsPerSecondLabel = [SKLabelNode labelNodeWithFontNamed:fontName];
    bugsPerSecondLabel.fontSize = 20.0;
    bugsPerSecondLabel.text = [NSString stringWithFormat:@"Bugs Per Second: %ld", (long)[GameData sharedData].bugsPerSecond];
    bugsPerSecondLabel.name = @"scoreLabel";
    bugsPerSecondLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    bugsPerSecondLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
    bugsPerSecondLabel.position = CGPointMake(CGRectGetMidX(self.frame), self.size.height - bugsLabel.frame.size.height * 2);
    [hudLayerNode addChild:bugsPerSecondLabel];
}

- (void)startTimer {
    if([timer isValid]) {
        [timer invalidate];
        timer = nil;
    }
    
    timer = [NSTimer scheduledTimerWithTimeInterval: 1
                                             target: self
                                           selector: @selector(getBugsEverySecond)
                                           userInfo: nil
                                            repeats: YES];
}

- (void)stopTimer {
    if([timer isValid]) {
        [timer invalidate];
        timer = nil;
    }
}

- (void)getBugsEverySecond {
    if ([GameData sharedData].bugsPerSecond > 0) {
        [GameData sharedData].bugs = [GameData sharedData].bugs + [GameData sharedData].bugsPerSecond;
        [bugsLabel runAction:[self updateBugsLabel]];
        [self makeBugsFallFromButton:YES OnTap:NO];
        NSLog(@"Bugs Added: %ld", (long)[GameData sharedData].bugsPerSecond);
    }
}

- (SKAction *)updateBugsLabel {
    SKAction *flashAction = [SKAction sequence:@[[SKAction scaleTo:1.1 duration:0.1],
                                                 [SKAction scaleTo:1.0 duration:0.1],
                                                 [SKAction runBlock:^{
        bugsLabel.text = [NSString stringWithFormat:@"Bugs Squashed: %ld", (long)[GameData sharedData].bugs];
    }]]];
    
    return flashAction;
}

- (SKButtonNode *)squashBugsButton {
    squashButton = [[SKButtonNode alloc] initWithTextureNormal:[textures textureNamed:@"debugBtnNormal"]
                                                      selected:[textures textureNamed:@"debugBtnSelected"]
                                                      disabled:nil];
    
    squashButton.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    squashButton.zPosition = 10;
    [squashButton setTouchUpInsideTarget:self action:@selector(buttonTapped)];
    
    return squashButton;
}

- (void)buttonTapped {
    [GameData sharedData].bugs = [GameData sharedData].bugs + [GameData sharedData].bugsPerTap;
    [bugsLabel runAction:[self updateBugsLabel]];
    [self showBugsTapped:[GameData sharedData].bugsPerTap];
    [self makeBugsFallFromButton:YES OnTap:YES];
    [squashButton runAction:[SKAction playSoundFileNamed:@"tap.caf" waitForCompletion:NO]];
}

static inline CGFloat skRandf() {
    return rand() / (CGFloat) RAND_MAX;
}

static inline CGFloat skRand(CGFloat low, CGFloat high) {
    return skRandf() * (high - low) + low;
}

- (void)showBugsTapped:(NSInteger)bugs {
    SKLabelNode *bugsTappedLabel = [SKLabelNode labelNodeWithFontNamed:fontName];
    bugsTappedLabel.fontSize = 10;
    bugsTappedLabel.text = [NSString stringWithFormat:@"+%ld", (long)bugs];
    bugsTappedLabel.position = CGPointMake(skRand(CGRectGetMidX(self.frame)-60, CGRectGetMidX(self.frame)+60), CGRectGetMidY(self.frame));
    bugsTappedLabel.fontColor = [SKColor blackColor];
    [self addChild:bugsTappedLabel];
    
    SKAction *moveUp = [SKAction moveByX:0 y:150 duration:0.7];
    SKAction *fade = [SKAction fadeAlphaTo:0 duration:0.7];
    SKAction *scale = [SKAction scaleTo:3 duration:0.7];
    SKAction *remove = [SKAction removeFromParent];
    [bugsTappedLabel runAction:[SKAction group:@[moveUp, fade, scale]] completion:^{
        [bugsTappedLabel runAction:remove];
    }];
}

- (void)makeBugsFallFromButton:(BOOL)fall OnTap:(BOOL)onTap {
    if (!onTap) {
        for (int i = [GameData sharedData].bugsPerSecond * .01; i < [GameData sharedData].bugsPerSecond *.01002; i++) {
            SKSpriteNode *bug = [SKSpriteNode spriteNodeWithTexture:[textures textureNamed:@"bug"]];
            bug.position = CGPointMake(skRand(CGRectGetMidX(self.frame)-45, CGRectGetMidX(self.frame)+45), CGRectGetMidY(self.frame));
            bug.name = @"bug";
            bug.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:10];
            bug.physicsBody.allowsRotation = YES;
            [self addChild:bug];
        }
    } else if (onTap) {
        for (int i = [GameData sharedData].bugsPerTap * .01; i < [GameData sharedData].bugsPerTap *.0100001; i++) {
            SKSpriteNode *bug = [SKSpriteNode spriteNodeWithTexture:[textures textureNamed:@"bug"]];
            bug.position = CGPointMake(skRand(CGRectGetMidX(self.frame)-45, CGRectGetMidX(self.frame)+45), CGRectGetMidY(self.frame));
            bug.name = @"bug";
            bug.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:10];
            bug.physicsBody.allowsRotation = YES;
            [self addChild:bug];
        }
    }
}

- (SKButtonNode *)hireStaffButton {
    SKButtonNode *hireStaff = [[SKButtonNode alloc] initWithTextureNormal:[textures textureNamed:@"squashBtnUp"]
                                                                 selected:[textures textureNamed:@"squashBtnDown"]
                                                                 disabled:[textures textureNamed:@"squashBtnDisabled"]];
    
    hireStaff.size = CGSizeMake(120, 50);
    hireStaff.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:hireStaff.size];
    hireStaff.physicsBody.dynamic = NO;
    hireStaff.position = CGPointMake((hireStaff.size.width/2) + 5, 50);
    [hireStaff setTouchUpInsideTarget:self action:@selector(goToHireStaffScene)];
    
    SKLabelNode *buttonText = [SKLabelNode labelNodeWithFontNamed:fontName];
    buttonText.text = @"Hire Staff";
    buttonText.fontSize = 20;
    buttonText.fontColor = [SKColor whiteColor];
    buttonText.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    buttonText.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
    
    [hireStaff addChild:buttonText];
    
    return hireStaff;
}

- (SKButtonNode *)upgradeButton {
    SKButtonNode *upgradeButton = [[SKButtonNode alloc] initWithTextureNormal:[textures textureNamed:@"squashBtnUp"]
                                                                     selected:[textures textureNamed:@"squashBtnDown"]
                                                                     disabled:[textures textureNamed:@"squashBtnDisabled"]];
    
    upgradeButton.size = CGSizeMake(120, 50);
    upgradeButton.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:upgradeButton.size];
    upgradeButton.physicsBody.dynamic = NO;
    upgradeButton.position = CGPointMake(self.frame.size.width - (upgradeButton.size.width/2) - 5, 50);
    [upgradeButton setTouchUpInsideTarget:self action:@selector(goToUpgradeScene)];
    
    SKLabelNode *buttonText = [SKLabelNode labelNodeWithFontNamed:fontName];
    buttonText.text = @"Upgrades";
    buttonText.fontSize = 20;
    buttonText.fontColor = [SKColor whiteColor];
    buttonText.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    buttonText.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
    
    [upgradeButton addChild:buttonText];
    
    return upgradeButton;
}

- (void)goToHireStaffScene {
    [self runAction:[SKAction playSoundFileNamed:@"slideUp.caf" waitForCompletion:NO]];
    [self stopTimer];
    
    SKScene *hireStaffScene = [[HireStaff alloc] initWithSize:self.size];
    SKTransition *slideUp = [SKTransition pushWithDirection:SKTransitionDirectionUp duration:0.5];
    [self.view presentScene:hireStaffScene transition:slideUp];
}

- (void)goToUpgradeScene {
    [self stopTimer];
    [self runAction:[SKAction playSoundFileNamed:@"slideUp.caf" waitForCompletion:NO]];
    
    SKScene *upgradesScene = [[Upgrades alloc] initWithSize:self.size];
    SKTransition *slideUp = [SKTransition pushWithDirection:SKTransitionDirectionUp duration:0.5];
    [self.view presentScene:upgradesScene transition:slideUp];
}

- (void)didSimulatePhysics
{
    [self enumerateChildNodesWithName:@"bug" usingBlock:^(SKNode *node, BOOL *stop) {
        if (node.position.y < 0)
            [node removeFromParent];
    }];
}

@end
