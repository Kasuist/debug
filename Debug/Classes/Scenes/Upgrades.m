//
//  Upgrades.m
//  Debug
//
//  Created by Beau Young on 25/01/2014.
//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import "Upgrades.h"
#import "SKButtonNode.h"
#import "PurchaseItemButton.h"
#import "Game.h"
#import "GameData.h"

#define kHUD_BAR_HEIGHT 30

static NSString *fontName = @"PF Arma Five";

@implementation Upgrades {
    SKTextureAtlas *textures;
    
    SKNode *hudLayerNode;
    SKLabelNode *cashLabel;
    
    PurchaseItemButton *buyAWhipButton;
}


- (void)didMoveToView:(SKView *)view {
    self.scaleMode = SKSceneScaleModeAspectFill;
    self.backgroundColor = [SKColor whiteColor];
    
    textures = [SKTextureAtlas atlasNamed:@"game"];
    [self createSceneContents];
}

- (void)createSceneContents {
    hudLayerNode = [SKNode node];
    [self addChild:hudLayerNode];
    
    [self addChild:[self whipButton]];
    [self addChild:[self closeButton]];
    
    [self setupUI];
}

- (void)setupUI {
    int barHeight = kHUD_BAR_HEIGHT;
    CGSize backgroundSize = CGSizeMake(self.size.width, barHeight);
    SKSpriteNode *hudBarBackground = [SKSpriteNode spriteNodeWithColor:[UIColor colorWithRed:0.103 green:0.529 blue:0.882 alpha:0.740] size:backgroundSize];
    hudBarBackground.position = CGPointMake(0, self.size.height - barHeight);
    hudBarBackground.anchorPoint = CGPointZero;
    [hudLayerNode addChild:hudBarBackground];
    
    cashLabel = [SKLabelNode labelNodeWithFontNamed:fontName];
    cashLabel.fontSize = 20.0;
    cashLabel.text = [NSString stringWithFormat:@"Cash: $%ld", (long)[GameData sharedData].bugs];
    cashLabel.name = @"scoreLabel";
    cashLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    cashLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
    cashLabel.position = CGPointMake(CGRectGetMidX(self.frame), self.size.height - cashLabel.frame.size.height);
    
    [hudLayerNode addChild:cashLabel];
}

- (SKButtonNode *)whipButton {
    buyAWhipButton = [[PurchaseItemButton alloc] initWithTextureNormal:[textures textureNamed:@"squashBtnUp"]
                                                              selected:[textures textureNamed:@"squashBtnDown"]
                                                              disabled:[textures textureNamed:@"squashBtnDisabled"]];
    
    [buyAWhipButton setTouchUpInsideTarget:self action:@selector(buyWhip)];
    
    buyAWhipButton.size = CGSizeMake(240, 50);
    buyAWhipButton.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:buyAWhipButton.size];
    buyAWhipButton.physicsBody.dynamic = NO;
    buyAWhipButton.titleString = [NSString stringWithFormat:@"Upgrade Whip: $%ld", (long)[GameData sharedData].whipPrice];
    buyAWhipButton.position = CGPointMake(buyAWhipButton.size.width/2, 300);

    [self checkIfButtonShouldBeDisabled];
    
    return buyAWhipButton;
}
- (void)buyWhip {
    [GameData sharedData].bugs = [GameData sharedData].bugs - [GameData sharedData].whipPrice;
    [[GameData sharedData] calculateBugsPerTap];
    
    cashLabel.text = [NSString stringWithFormat:@"Cash: $%ld", (long)[GameData sharedData].bugs];
    [self checkIfButtonShouldBeDisabled];
    [self runAction:[self playChingSound]];
    [self removeButton:buyAWhipButton];
}

- (void)checkIfButtonShouldBeDisabled {
    if ([GameData sharedData].bugs < [GameData sharedData].whipPrice) {
        buyAWhipButton.isEnabled = NO;
    }
}

- (SKButtonNode *)closeButton {
    SKButtonNode *button = [[SKButtonNode alloc] initWithTextureNormal:[textures textureNamed:@"squashBtnUp"] selected:[textures textureNamed:@"squashBtnDown"]];
    button.size = CGSizeMake(100, 50);
    button.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:button.size];
    button.physicsBody.dynamic = NO;
    button.position = CGPointMake(CGRectGetMidX(self.frame), 50);
    [button setTouchUpInsideTarget:self action:@selector(goToGameScene)];
    
    SKLabelNode *buttonText = [SKLabelNode labelNodeWithFontNamed:fontName];
    buttonText.text = @"Back";
    buttonText.fontSize = 20;
    buttonText.fontColor = [SKColor whiteColor];
    buttonText.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    buttonText.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
    
    [button addChild:buttonText];
    
    return button;
}
- (void)goToGameScene {
    [self runAction:[SKAction playSoundFileNamed:@"slideDown.caf" waitForCompletion:NO]];

    SKScene *options = [[Game alloc] initWithSize:self.size];
    SKTransition *slideDown = [SKTransition pushWithDirection:SKTransitionDirectionDown duration:0.5];
    [self.view presentScene:options transition:slideDown];
}

- (SKAction *)playChingSound {
    SKAction *ching = [SKAction playSoundFileNamed:@"ching.caf" waitForCompletion:NO];
    return ching;
}

- (void)removeButton:(SKButtonNode *)sender {
    SKAction *slideLeft = [SKAction moveByX:-600 y:0 duration:0.5];
    [sender runAction:slideLeft completion:^{
        [sender removeFromParent];
    }];
}

@end
