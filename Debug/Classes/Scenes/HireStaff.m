//
//  Upgrades.m
//  Debug
//
//  Created by Beau Young on 25/01/2014.
//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import "HireStaff.h"
#import "SKButtonNode.h"
#import "Game.h"
#import "GameData.h"

#define kHUD_BAR_HEIGHT 30

static NSString *fontName = @"PF Arma Five";

@implementation HireStaff {
    SKTextureAtlas *textures;
    
    SKNode *hudLayerNode;
    SKAction *updateCash;
    
    SKLabelNode *cashLabel;
    
    SKButtonNode *trainMonkeyButton;
    SKButtonNode *hireInternButton;
}

- (void)didMoveToView:(SKView *)view {
    self.scaleMode = SKSceneScaleModeAspectFill;
    self.backgroundColor = [SKColor whiteColor];
    
    textures = [SKTextureAtlas atlasNamed:@"game"];
    [self createSceneContents];
}

- (void)createSceneContents {
    hudLayerNode = [SKNode node];
    [self addChild:hudLayerNode];
    
    [self addChild:[self trainMonkeyButton]];
    [self addChild:[self hireAnIntern]];
    [self addChild:[self closeButton]];
    
    [self setupUI];
}

- (void)setupUI {
    int barHeight = kHUD_BAR_HEIGHT;
    CGSize backgroundSize = CGSizeMake(self.size.width, barHeight);
    SKSpriteNode *hudBarBackground = [SKSpriteNode spriteNodeWithColor:[UIColor colorWithRed:0.103 green:0.529 blue:0.882 alpha:0.740] size:backgroundSize];
    hudBarBackground.position = CGPointMake(0, self.size.height - barHeight);
    hudBarBackground.anchorPoint = CGPointZero;
    [hudLayerNode addChild:hudBarBackground];
    
    cashLabel = [SKLabelNode labelNodeWithFontNamed:fontName];
    cashLabel.fontSize = 20.0;
    cashLabel.text = [NSString stringWithFormat:@"Cash: $%ld", (long)[GameData sharedData].bugs];
    cashLabel.name = @"scoreLabel";
    cashLabel.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    cashLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
    cashLabel.position = CGPointMake(CGRectGetMidX(self.frame), self.size.height - cashLabel.frame.size.height);
    
    [hudLayerNode addChild:cashLabel];
}
- (void)setCashLabel {
    cashLabel.text = [NSString stringWithFormat:@"Cash: $%ld", (long)[GameData sharedData].bugs];
}

- (SKButtonNode *)trainMonkeyButton {
    trainMonkeyButton = [[SKButtonNode alloc] initWithTextureNormal:[textures textureNamed:@"squashBtnUp"]
                                                           selected:[textures textureNamed:@"squashBtnDown"]
                                                           disabled:[textures textureNamed:@"squashBtnDisabled"]];
    
    [trainMonkeyButton setTouchUpInsideTarget:self action:@selector(addMonkey)];
    
    trainMonkeyButton.size = CGSizeMake(250, 50);
    trainMonkeyButton.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:trainMonkeyButton.size];
    trainMonkeyButton.physicsBody.dynamic = NO;
    trainMonkeyButton.position = CGPointMake(trainMonkeyButton.size.width/2, self.frame.size.height - 100);
    
    trainMonkeyButton.title = [SKLabelNode labelNodeWithFontNamed:fontName];
    trainMonkeyButton.title.text = [NSString stringWithFormat:@"Train a Monkey: $%ld", (long)[GameData sharedData].monkeyPrice];
    trainMonkeyButton.title.fontSize = 15;
    trainMonkeyButton.title.fontColor = [SKColor whiteColor];
    trainMonkeyButton.title.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    trainMonkeyButton.title.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
    
    [self checkIfButtonShouldBeDisabled];
    
    [trainMonkeyButton addChild:trainMonkeyButton.title];
    
    return trainMonkeyButton;
}

- (void)addMonkey {
    [GameData sharedData].bugs = [GameData sharedData].bugs - [GameData sharedData].monkeyPrice;
    [GameData sharedData].monkeysHired = [GameData sharedData].monkeysHired + 1;
    [[GameData sharedData] calculateBugsPerSecond];
    [self checkIfButtonShouldBeDisabled];

    [self setCashLabel];
    trainMonkeyButton.title.text = [NSString stringWithFormat:@"Train a Monkey: $%ld", (long)[GameData sharedData].monkeyPrice];
    [self runAction:[self playChingSound]];
}

- (void)checkIfButtonShouldBeDisabled {
    if ([GameData sharedData].bugs < [GameData sharedData].monkeyPrice) {
        trainMonkeyButton.isEnabled = NO;
    }
    if ([GameData sharedData].bugs < [GameData sharedData].internPrice) {
        hireInternButton.isEnabled = NO;
    }
}

- (SKButtonNode *)hireAnIntern {
    hireInternButton = [[SKButtonNode alloc] initWithTextureNormal:[textures textureNamed:@"squashBtnUp"]
                                                          selected:[textures textureNamed:@"squashBtnDown"]
                                                          disabled:[textures textureNamed:@"squashBtnDisabled"]];
    hireInternButton.size = CGSizeMake(240, 50);
    hireInternButton.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:hireInternButton.size];
    hireInternButton.physicsBody.dynamic = NO;
    hireInternButton.position = CGPointMake(hireInternButton.size.width/2, trainMonkeyButton.position.y - 60);
    [hireInternButton setTouchUpInsideTarget:self action:@selector(addIntern)];
    
    hireInternButton.title = [SKLabelNode labelNodeWithFontNamed:fontName];
    hireInternButton.title.text = [NSString stringWithFormat:@"Hire an Intern: $%ld", (long)[GameData sharedData].internPrice];
    hireInternButton.title.fontSize = 15;
    hireInternButton.title.fontColor = [SKColor whiteColor];
    hireInternButton.title.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    hireInternButton.title.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
    
    [self checkIfButtonShouldBeDisabled];
    [hireInternButton addChild:hireInternButton.title];
    
    return hireInternButton;
}


- (void)addIntern {
    [GameData sharedData].bugs = [GameData sharedData].bugs - [GameData sharedData].internPrice;
    [GameData sharedData].internsHired = [GameData sharedData].internsHired + 1;
    [[GameData sharedData] calculateBugsPerSecond];
    [self checkIfButtonShouldBeDisabled];
    
    [self setCashLabel];
    hireInternButton.title.text = [NSString stringWithFormat:@"Hire an Intern: $%ld", (long)[GameData sharedData].internPrice];
    [self runAction:[self playChingSound]];
}

- (SKButtonNode *)closeButton {
    SKButtonNode *button = [[SKButtonNode alloc] initWithTextureNormal:[textures textureNamed:@"squashBtnUp"]
                                                              selected:[textures textureNamed:@"squashBtnDown"]];
    
    button.size = CGSizeMake(100, 50);
    button.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:button.size];
    button.physicsBody.dynamic = NO;
    button.position = CGPointMake(CGRectGetMidX(self.frame), 50);
    [button setTouchUpInsideTarget:self action:@selector(goToGameScene)];
    
    SKLabelNode *buttonText = [SKLabelNode labelNodeWithFontNamed:fontName];
    buttonText.text = @"Back";
    buttonText.fontSize = 20;
    buttonText.fontColor = [SKColor whiteColor];
    buttonText.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
    buttonText.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
    
    [button addChild:buttonText];
    
    return button;
}
- (void)goToGameScene {
    [self runAction:[SKAction playSoundFileNamed:@"slideDown.caf" waitForCompletion:NO]];
    
    SKScene *options = [[Game alloc] initWithSize:self.size];
    SKTransition *slideDown = [SKTransition pushWithDirection:SKTransitionDirectionDown duration:0.5];
    [self.view presentScene:options transition:slideDown];
}

- (SKAction *)playChingSound {
    SKAction *ching = [SKAction playSoundFileNamed:@"ching.caf" waitForCompletion:NO];
    return ching;
}

@end
