//
//  TextureAtlasHelper.m
//  TrashPack3
//
//  Created by Beau Young on 16/01/2014.
//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import "TextureAtlasHelper.h"

#define IS_WIDESCREEN ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

@implementation TextureAtlasHelper

+ (SKTextureAtlas *)getTexturesAtlasWithName:(NSString *)atlasName {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        if (IS_WIDESCREEN) {
            atlasName = [NSString stringWithFormat:@"%@-568", atlasName];
        } else {
            atlasName = atlasName;
        }
    } else {
        atlasName = [NSString stringWithFormat:@"%@-ipad", atlasName];
    }
    
    SKTextureAtlas *textureAtlas = [SKTextureAtlas atlasNamed:atlasName];
    
    return textureAtlas;
}




@end
