//
//  GameData.m
//  Debug
//
//  Created by Beau Young on 24/01/2014.
//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import "GameData.h"

#define INTERN_PRICE_MULITPLIER 1.23
#define MONKEY_PRICE_MULTIPLIER 1.10
#define WHIP_PRICE_MULTIPLIER 1.32

@implementation GameData

#pragma mark - init
static GameData *sharedData = nil;
+ (GameData *)sharedData {
    
    if (!sharedData) {
        sharedData = [[GameData alloc] init];
    }
    return sharedData;
}

- (id)init {
    if (self = [super init]) {
        
        _bugs = [[NSUserDefaults standardUserDefaults] integerForKey:@"bugs"];
        _bugsPerTap = [[NSUserDefaults standardUserDefaults] integerForKey:@"bugsPerTap"];
        _bugsPerSecond = [[NSUserDefaults standardUserDefaults] integerForKey:@"bugsPerSecond"];
        _startingBugsPerTap = [[NSUserDefaults standardUserDefaults] integerForKey:@"startingBugsPerTap"];
        
        // upgrades
        _whipPrice = [[NSUserDefaults standardUserDefaults] integerForKey:@"whipPrice"];
        _bugsPerWhip = [[NSUserDefaults standardUserDefaults] integerForKey:@"bugsPerWhip"];
        _whipsPurchased = [[NSUserDefaults standardUserDefaults] integerForKey:@"whipsPurchased"];
        
        // staff
        _monkeyPrice = [[NSUserDefaults standardUserDefaults] integerForKey:@"monkeyPrice"];
        _monkeysHired = [[NSUserDefaults standardUserDefaults] integerForKey:@"monkeysHired"];
        _bugsPerMonkey = [[NSUserDefaults standardUserDefaults] integerForKey:@"bugsPerMonkey"];
        
        _internPrice = [[NSUserDefaults standardUserDefaults] integerForKey:@"internPrice"];
        _internsHired = [[NSUserDefaults standardUserDefaults] integerForKey:@"internsHired"];
        _bugsPerIntern = [[NSUserDefaults standardUserDefaults] integerForKey:@"bugsPerIntern"];
        
        [self calculateBugsPerTap];
        [self calculateBugsPerSecond];
    }
    return self;
}

- (void)calculateBugsPerTap {
    NSInteger bugsForAllWhips = _bugsPerWhip * _whipsPurchased;
    // insert new upgrades here
    
    
    
    NSInteger newBugsPerTap = _startingBugsPerTap + bugsForAllWhips; // plus other upgrades
    
    [self setBugsPerTap:newBugsPerTap];
}

- (void)calculateBugsPerSecond {
    NSInteger bugsForAllMonkeys = _bugsPerMonkey * _monkeysHired;
    NSInteger bugsForAllInterns = _bugsPerIntern * _internsHired;

    NSInteger bugsPerSecond = bugsForAllMonkeys + bugsForAllInterns;
    
    [self setBugsPerSecond:bugsPerSecond];
}

#pragma mark - Setters
- (void)setBugs:(NSInteger)bugs {
    [[NSUserDefaults standardUserDefaults] setInteger:bugs forKey:@"bugs"];
    _bugs = bugs;
}

- (void)setBugsPerSecond:(NSInteger)bugsPerSecond {
    [[NSUserDefaults standardUserDefaults] setInteger:bugsPerSecond forKey:@"bugsPerSecond"];
    _bugsPerSecond = bugsPerSecond;
}

- (void)setBugsPerTap:(NSInteger)bugsPerTap {
    [[NSUserDefaults standardUserDefaults] setInteger:bugsPerTap forKey:@"bugsPerTap"];
    _bugsPerTap = bugsPerTap;
}

- (void)setMonkeysHired:(NSInteger)monkeysHired {
    [[NSUserDefaults standardUserDefaults] setInteger:monkeysHired forKey:@"monkeysHired"];
    _monkeysHired = monkeysHired;
    self.monkeyPrice = _monkeyPrice * MONKEY_PRICE_MULTIPLIER;
}

- (void)setMonkeyPrice:(NSInteger)monkeyPrice {
    [[NSUserDefaults standardUserDefaults] setInteger:monkeyPrice forKey:@"monkeyPrice"];
    _monkeyPrice = monkeyPrice;
}
- (void)setInternsHired:(NSInteger)internsHired {
    [[NSUserDefaults standardUserDefaults] setInteger:internsHired forKey:@"internsHired"];
    _internsHired = internsHired;
    self.internPrice = _internPrice * INTERN_PRICE_MULITPLIER;
}

- (void)setInternPrice:(NSInteger)internPrice {
    [[NSUserDefaults standardUserDefaults] setInteger:internPrice forKey:@"internPrice"];
    _internPrice = internPrice;
}

- (void)setWhipsPurchased:(NSInteger)whipsPurchased {
    [[NSUserDefaults standardUserDefaults] setInteger:whipsPurchased forKey:@"whipsPurchased"];
    _whipsPurchased = whipsPurchased;
    self.whipPrice = _whipPrice * WHIP_PRICE_MULTIPLIER;
}

- (void)setWhipPrice:(NSInteger)whipPrice {
    [[NSUserDefaults standardUserDefaults] setInteger:whipPrice forKey:@"whipPrice"];
    _whipPrice = whipPrice;
}

@end
