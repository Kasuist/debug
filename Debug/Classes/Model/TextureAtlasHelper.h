//
//  TextureAtlasHelper.h
//  TrashPack3
//
//  Created by Beau Young on 16/01/2014.
//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import <float.h>

@interface TextureAtlasHelper : NSObject

+ (SKTextureAtlas *)getTexturesAtlasWithName:(NSString *)atlasName;

@end
