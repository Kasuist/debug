//
//  GameData.h
//  Debug
//
//  Created by Beau Young on 24/01/2014.
//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GameData : NSObject

@property (nonatomic) NSInteger bugs;
@property (nonatomic) NSInteger bugsPerTap;
@property (nonatomic) NSInteger bugsPerSecond;
@property (nonatomic) NSInteger startingBugsPerTap;

@property (nonatomic) NSInteger whipPrice;
@property (nonatomic) NSInteger whipsPurchased;
@property (nonatomic) NSInteger bugsPerWhip;

@property (nonatomic) NSInteger monkeyPrice;
@property (nonatomic) NSInteger monkeysHired;
@property (nonatomic) NSInteger bugsPerMonkey;

@property (nonatomic) NSInteger internPrice;
@property (nonatomic) NSInteger internsHired;
@property (nonatomic) NSInteger bugsPerIntern;

+ (GameData *)sharedData;

- (void)calculateBugsPerTap;
- (void)calculateBugsPerSecond;

@end
