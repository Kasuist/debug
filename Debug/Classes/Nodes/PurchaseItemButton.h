//
//  PurchaseItemButton.h
//  Debug
//
//  Created by Beau Young on 1/03/2014.
//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import "SKButtonNode.h"

@interface PurchaseItemButton : SKButtonNode

@property (strong, nonatomic) NSString *titleString;
@property (nonatomic) NSInteger *price;

- (void)removeWithDuration:(float)duration;

@end
