//
//  PurchaseItemButton.m
//  Debug
//
//  Created by Beau Young on 1/03/2014.
//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import "PurchaseItemButton.h"

@implementation PurchaseItemButton

static NSString *fontName = @"PF Arma Five";

- (id)initWithTextureNormal:(SKTexture *)normal selected:(SKTexture *)selected disabled:(SKTexture *)disabled {
    self = [super initWithTextureNormal:normal selected:selected disabled:disabled];
    if (self) {
        
        self.title = [SKLabelNode labelNodeWithFontNamed:fontName];
        self.title.fontSize = 15.0;
        self.title.fontColor = [SKColor whiteColor];
        self.title.verticalAlignmentMode = SKLabelVerticalAlignmentModeCenter;
        self.title.horizontalAlignmentMode = SKLabelHorizontalAlignmentModeCenter;
        
        [self addChild:self.title];
        
    }
    return self;
}

- (void)removeWithDuration:(float)duration {
    SKAction *slideLeft = [SKAction moveByX:-600 y:0 duration:duration];
    [self runAction:slideLeft completion:^{
        [self removeFromParent];
    }];
}

- (void)setTitleString:(NSString *)titleString {
    self.title.text = titleString;
    CGSize size = CGSizeMake(self.title.frame.size.width + 20, self.title.frame.size.height + 20);
    [self setSize:size];
}

@end
