//
//  ViewController.m
//  Debug
//
//  Created by Beau Young on 24/01/2014.
//  Copyright (c) 2014 Beau Young. All rights reserved.
//

#import "ViewController.h"
#import "Game.h"
#import "GameData.h"

@implementation ViewController

- (void)viewWillLayoutSubviews
{
    [super viewDidLoad];

    // Configure the view.
    SKView * skView = (SKView *)self.view;
    if (!skView.scene) {
        skView.showsFPS = YES;
        skView.showsNodeCount = YES;
        
        [GameData sharedData];
        
        // Create and configure the scene.
        SKScene * scene = [Game sceneWithSize:skView.bounds.size];
        scene.scaleMode = SKSceneScaleModeAspectFill;
        
        // Present the scene.
        [skView presentScene:scene];
    }
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    } else {
        return UIInterfaceOrientationMaskAll;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

@end
